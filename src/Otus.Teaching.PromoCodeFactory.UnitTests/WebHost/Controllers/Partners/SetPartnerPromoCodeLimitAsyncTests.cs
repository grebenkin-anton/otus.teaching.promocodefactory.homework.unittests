﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerNotFound_ShouldReturnNotFound()
        {
            //Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerIsNotActive_ShouldReturnBadRequest()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = false
            };
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.StatusCode.Should().Be(400);
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Данный партнер не активен");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfHasActiveLimit_ShouldSetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var activeLimit = new PartnerPromoCodeLimit();
            var partner = new Partner()
            {
                IsActive = true,
                NumberIssuedPromoCodes = 123,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    activeLimit
                }
            };
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
            activeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfHasNotActiveLimit_ShouldNotSetPartnerNumberIssuedPromoCodesToZero()
        {
            //Arrange
            var canceledLimit = new PartnerPromoCodeLimit()
            {
                CancelDate = DateTime.Now
            };
            var partner = new Partner()
            {
                IsActive = true,
                NumberIssuedPromoCodes = 123,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    canceledLimit
                }
            };
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, new SetPartnerPromoCodeLimitRequest());

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(123);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitIs0_ShouldReturnBadRequest()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>().Which.Value.Should().Be("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsSavedToDatabase()
        {
            //Arrange
            var partner = new Partner()
            {
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 1
            };
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.Empty, setPartnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.Count.Should().Be(1);
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }

    }
}